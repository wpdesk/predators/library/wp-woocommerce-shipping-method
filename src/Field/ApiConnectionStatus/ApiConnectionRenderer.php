<?php

namespace WPDesk\WooCommerceShippingMethod\Field\ApiConnectionStatus;

trait ApiConnectionRenderer {

	/**
	 * Generate Api Connection Status HTML.
	 *
	 * @param string $key Field key.
	 * @param array  $data Field data.
	 * @return string
	 */
	public function generate_api_connection_status_html( $key, $data ) {
		$field = new ApiConnectionStatus();
		ob_start();
		$field->output_connection_status( $key, $data, $this->id );

		return ob_get_clean();
	}

}
