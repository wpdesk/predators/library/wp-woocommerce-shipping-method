<?php

namespace WPDesk\WooCommerceShippingMethod\Field\ApiConnectionStatus;

/**
 * Can render settings field.
 */
class ApiConnectionStatus {

	const FIELD_TYPE = 'api_connection_status';

	/**
	 * @param string $key .
	 * @param string[] $data .
	 * @param string $method_id .
	 *s
	 * @return void
	 */
	public function output_connection_status( $key, $data, $method_id ) {
		$ajax = [
			'action' => ApiConnectionStatusAjax::AJAX_ACTION,
			'nonce'  => wp_create_nonce( ApiConnectionStatusAjax::AJAX_ACTION ),
			'url'    => admin_url( 'admin-ajax.php' ),
		];

		include __DIR__ . '/views/html-connection-status-field.php';
	}

}
