<?php

namespace WPDesk\WooCommerceShippingMethod\Field\ApiConnectionStatus;

interface HasApiConnectionStatusField {

	/**
	 * Generate Api Connection Status HTML.
	 *
	 * @param string $key Field key.
	 * @param array  $data Field data.
	 * @return string
	 */
	public function generate_api_connection_status_html( $key, $data );

}
