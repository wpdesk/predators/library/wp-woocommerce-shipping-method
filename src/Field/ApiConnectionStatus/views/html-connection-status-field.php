<?php
/**
 * @var string[] $data .
 * @var string[] $ajax  .
 * @var string $key .
 * @var string $method_id .
 *
 * @package WPDesk\GLS\WooCommerceSettings
 */

// @phpstan-ignore-next-line
defined( 'ABSPATH' ) || exit;

$field_id = esc_attr( \WPDesk\WooCommerceShippingMethod\Field\ApiConnectionStatus\ApiConnectionStatus::FIELD_TYPE . '_' . $key );
?>

<tr valign="top">
	<th scope="row" class="titledesc">
		<?php echo wp_kses_post( $data['title'] ); ?>
	</th>
	<td class="forminp forminp-text" id="<?php echo esc_attr( $field_id ); ?>">
		<strong class="js--result-field"><span style="float: left; margin: unset;" class="spinner is-active"></span></strong>

		<script type="text/javascript">
			document.addEventListener( "DOMContentLoaded", function ( event ) {
				var request = new XMLHttpRequest();
				var params = new FormData();
				params.append( 'integration', '<?php echo esc_js( $method_id ); ?>' );
				params.append( 'field', '<?php echo esc_js( $key ); ?>' );
				params.append( 'action', '<?php echo esc_js( $ajax['action'] ); ?>' );
				params.append( '_wpnonce', '<?php echo esc_js( $ajax['nonce'] ); ?>' );

				request.open( 'POST', '<?php echo esc_url( $ajax['url'] ); ?>', true );

				request.onload = function () {
					if ( this.status >= 200 && this.status < 400 ) {
						var response = JSON.parse( this.response );
						var elem = document.querySelector( '#<?php echo esc_attr( $field_id ); ?> .js--result-field' );

						if ( response.success ) {
							elem.style.color = 'green';
						} else {
							elem.style.color = 'red';
						}

						elem.innerHTML = response.data.message;
					}
				};

				request.send( params );
			} );
		</script>
	</td>
</tr>
