<?php

namespace WPDesk\WooCommerceShippingMethod\Field\ApiConnectionStatus;

/**
 * Can check Api Connection Status.
 */
abstract class ApiConnectionStatusAjax implements \WPDesk\PluginBuilder\Plugin\Hookable {

	const AJAX_ACTION = 'fs_api_connection_status';

	/**
	 * @var string
	 */
	private $integration;

	/**
	 * @param string $integration
	 */
	public function __construct( string $integration ) {
		$this->integration = $integration;
	}

	/**
	 * @inheritDoc
	 */
	public function hooks() {
		add_action( 'wp_ajax_' . self::AJAX_ACTION, [ $this, 'check_connection_status' ] );
	}

	/**
	 * @return void
	 */
	public function check_connection_status() {
		check_ajax_referer( self::AJAX_ACTION );

		$integration = isset( $_POST['integration'] ) ? sanitize_key( $_POST['integration'] ) : '';

		if ( $integration === $this->integration ) {
			try {
				$field = sanitize_key( $_POST['field'] );
				$this->check_api_connection( $field );
				wp_send_json_success( [ 'message' => __( 'Connected', 'wp-woocommerce-shipping-method' ) ] );
			} catch ( \Exception $e ) {
				wp_send_json_error(
					[
						'message' => sprintf(
						// Translators: error.
							__( 'Connection error: %1$s', 'wp-woocommerce-shipping-method' ),
							$e->getMessage()
						),
					]
				);
			}
		}
	}

	/**
	 * @param string $field .
	 * @return void
	 *
	 * @throws \Exception
	 */
	abstract public function check_api_connection( $field );

}
