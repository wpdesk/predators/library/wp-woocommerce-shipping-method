[![pipeline status](https://gitlab.com/wpdesk/wp-woocommerce-shipping-method/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-woocommerce-shipping-method/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-woocommerce-shipping-method/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-woocommerce-shipping-method/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-woocommerce-shipping-method/v/stable)](https://packagist.org/packages/wpdesk/wp-woocommerce-shipping-method) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-woocommerce-shipping-method/downloads)](https://packagist.org/packages/wpdesk/wp-woocommerce-shipping-method) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-woocommerce-shipping-method/v/unstable)](https://packagist.org/packages/wpdesk/wp-woocommerce-shipping-method) 
[![License](https://poser.pugx.org/wpdesk/wp-woocommerce-shipping-method/license)](https://packagist.org/packages/wpdesk/wp-woocommerce-shipping-method) 

WooCommerce Shipping Method
===========================

For Flexible Shipping Shipping Integrations

