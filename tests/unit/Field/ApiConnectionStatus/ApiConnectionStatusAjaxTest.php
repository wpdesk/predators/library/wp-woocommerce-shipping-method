<?php

namespace unit\Field\ApiConnectionStatus;

use WP_Mock\Tools\TestCase;
use WPDesk\WooCommerceShippingMethod\Field\ApiConnectionStatus\ApiConnectionStatusAjax;

class ApiConnectionStatusAjaxTest extends TestCase {


	/**
	 * @var \PHPUnit\Framework\MockObject\MockObject|ApiConnectionStatusAjax
	 */
	private $api_connection_status_ajax_under_tests;

	public function setUp() {
		\WP_Mock::setUp();

		$this->api_connection_status_ajax_under_tests = $this->getMockBuilder( ApiConnectionStatusAjax::class )
		                                                     ->setConstructorArgs( array( 'integration' ) )
		                                                     ->setMethods( array( 'check_api_connection' ) )
		                                                     ->getMock();
	}

	public function tearDown() {
		\WP_Mock::tearDown();
	}

	public function testShouldAddHooks() {
		// Expects
		\WP_Mock::expectActionAdded( 'wp_ajax_' . ApiConnectionStatusAjax::AJAX_ACTION, [ $this->api_connection_status_ajax_under_tests, 'check_connection_status' ] );

		// When
		$this->api_connection_status_ajax_under_tests->hooks();

		// Then
		$this->assertTrue( true );
	}

	public function testShouldSendSuccess() {
		// Expects
		\WP_Mock::userFunction( 'check_ajax_referer' )->with( 'fs_api_connection_status' )->andReturn( true );
		\WP_Mock::userFunction( 'sanitize_key' )->andReturnArg( 0 );
		\WP_Mock::userFunction( 'wp_send_json_success' )->andReturnArg( 0 );

		$this->api_connection_status_ajax_under_tests->expects( $this->once() )->method( 'check_api_connection' )->willReturn( '' );

		// Given
		$_POST['field'] = 'api_status';
		$_POST['integration'] = 'integration';

		// When
		$this->api_connection_status_ajax_under_tests->check_connection_status();

		// Then
		$this->assertTrue( true );
	}

	public function testShouldSendErrorOnException() {
		// Expects
		\WP_Mock::userFunction( 'check_ajax_referer' )->with( 'fs_api_connection_status' )->andReturn( true );
		\WP_Mock::userFunction( 'sanitize_key' )->andReturnArg( 0 );
		\WP_Mock::userFunction( 'wp_send_json_error' )->andReturnArg( 0 );

		$this->api_connection_status_ajax_under_tests->expects( $this->once() )->method( 'check_api_connection' )->willThrowException( new \Exception( 'test' ) );

		// Given
		$_POST['field'] = 'api_status';
		$_POST['integration'] = 'integration';

		// When
		$this->api_connection_status_ajax_under_tests->check_connection_status();

		// Then
		$this->assertTrue( true );
	}

}
