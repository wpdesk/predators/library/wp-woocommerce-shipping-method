<?php

namespace unit\Field\ApiConnectionStatus;

use WP_Mock\Tools\TestCase;
use WPDesk\WooCommerceShippingMethod\Field\ApiConnectionStatus\ApiConnectionStatus;

class ApiConnectionStatusTest extends TestCase {

	/**
	 * @var ApiConnectionStatus
	 */
	private $api_connection_status_under_tests;

	public function setUp() {
		\WP_Mock::setUp();

		$this->api_connection_status_under_tests = new ApiConnectionStatus();
	}

	public function tearDown() {
		\WP_Mock::tearDown();
	}

	public function testShouldRenderField() {
		// Expects
		$this->expectOutputString( <<<END

<tr valign="top">
	<th scope="row" class="titledesc">
		Api Status	</th>
	<td class="forminp forminp-text" id="api_connection_status_api_status">
		<strong class="js--result-field"><span style="float: left; margin: unset;" class="spinner is-active"></span></strong>

		<script type="text/javascript">
			document.addEventListener( "DOMContentLoaded", function ( event ) {
				var request = new XMLHttpRequest();
				var params = new FormData();
				params.append( 'integration', 'method' );
				params.append( 'field', 'api_status' );
				params.append( 'action', 'fs_api_connection_status' );
				params.append( '_wpnonce', 'nonce' );

				request.open( 'POST', 'admin-ajax.php', true );

				request.onload = function () {
					if ( this.status >= 200 && this.status < 400 ) {
						var response = JSON.parse( this.response );
						var elem = document.querySelector( '#api_connection_status_api_status .js--result-field' );

						if ( response.success ) {
							elem.style.color = 'green';
						} else {
							elem.style.color = 'red';
						}

						elem.innerHTML = response.data.message;
					}
				};

				request.send( params );
			} );
		</script>
	</td>
</tr>

END );

		\WP_Mock::userFunction( 'wp_create_nonce' )->andReturn( 'nonce' );
		\WP_Mock::userFunction( 'admin_url' )->andReturnArg( 0 );
		\WP_Mock::userFunction( 'wp_kses_post' )->andReturnArg( 0 );

		// Given
		$data = [
			'title' => 'Api Status'
		];

		// When
		$this->api_connection_status_under_tests->output_connection_status( 'api_status', $data, 'method' );

		// Then
		$this->assertTrue( true );
	}

}
