## [1.1.0] - 2022-04-20
### Changed
- wp-view version

## [1.0.0] - 2021-12-02
### Added
- initial version
